package com.devcamp.artistalbumapi.controller;

import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.model.Artist;
import com.devcamp.artistalbumapi.service.ArtistService;

@CrossOrigin // Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class ArtistController {
    // Tạo GET API “/artists” trong ArtistController có đầu ra là ArrayList<Artist>
    @Autowired
    private ArtistService artistService;

    @GetMapping("/artist")
    public ArrayList<Artist> getAllArtist() {
        ArrayList<Artist> allArt = artistService.getAllArt();
        return allArt;
    }

    // Tạo GET API “/artist-info” trong ArtistController truyền vào param artistId
    // từ request
    @GetMapping("/artist-info")
    public Artist getInfoArtist1(@RequestParam(name = "id") int artistId) {
        return artistService.getInfoArtict(artistId);
    }
    //Tạo GET API “/artists/{index}” trong ArtistController có đầu ra là Artist thực 
    //hiện trả ra Artist thứ index trong danh sách artist đã khai báo
    @GetMapping("/artists/{index}")
    public Artist getArtistIndex(@PathVariable(name = "index" ) int artistId) {
        return artistService.getArtistIndext(artistId);
    }

}
