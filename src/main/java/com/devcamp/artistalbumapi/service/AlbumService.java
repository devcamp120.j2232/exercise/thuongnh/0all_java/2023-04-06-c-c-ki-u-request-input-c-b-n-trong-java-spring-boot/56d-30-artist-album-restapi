package com.devcamp.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.model.Album;

@Service
public class AlbumService {
    
    //   khởi tạo 9 abuml
    Album album1 = new Album(1, "vol1");
    Album album2 = new Album(2, "vol2");
    Album album3 = new Album(3, "vol3");

    Album album4 = new Album(4, "vol4");
    Album album5 = new Album(5, "vol5");
    Album album6 = new Album(6, "vol6");

    Album album7 = new Album(7, "vol7");
    Album album8 = new Album(8, "vol1");
    Album album9 = new Album(9, "vol9");

    // gom các albums vào arrayList để thêm vào đối tượng nghệ sỹ 
    // ARR Artict thương
    public ArrayList<Album> getArtThuong() {
        // khởi tạo đối tượng ARRlist  
        ArrayList<Album> albumThuong = new ArrayList<Album>();
        // thêm các đối tượng albums vào Arrlist
        albumThuong.add(album1);
        albumThuong.add(album2);
        albumThuong.add(album3);
        return albumThuong;

    }
     // ARR Artict thương
     public ArrayList<Album> getArtHuy() {
        // khởi tạo đối tượng ARRlist  
        ArrayList<Album> albumHuy = new ArrayList<Album>();
        // thêm các đối tượng albums vào Arrlist
        albumHuy.add(album4);
        albumHuy.add(album5);
        albumHuy.add(album6);
        return albumHuy;

    }
     // ARR Artict thương
     public ArrayList<Album> getArtTram() {
        // khởi tạo đối tượng ARRlist  
        ArrayList<Album> albumTram = new ArrayList<Album>();
        // thêm các đối tượng albums vào Arrlist
        albumTram.add(album7);
        albumTram.add(album8);
        albumTram.add(album9);
        return albumTram;

    }

    //  tất cả các albums
    public Album getFilterAlbumAll(int albumId) {
        // khởi tạo đối tượng ARRlist  
        ArrayList<Album> albums = new ArrayList<Album>();
        // thêm các đối tượng albums vào Arrlist
        albums.add(album1);
        albums.add(album2);
        albums.add(album3);
        albums.add(album4);
        albums.add(album5);
        albums.add(album6);
        albums.add(album7);
        albums.add(album8);
        albums.add(album9);
        Album findAlbum = new Album();

        // lọc blbum theo albumid 
        for ( Album albumElement : albums ){
            if( albumElement.getId() == albumId){
                findAlbum = albumElement;
            }
        }
        return findAlbum;

    }

}
