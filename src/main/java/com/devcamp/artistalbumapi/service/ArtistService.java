package com.devcamp.artistalbumapi.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.devcamp.artistalbumapi.model.Artist;

@Service
public class ArtistService {
    @Autowired
    private AlbumService albumService;

    Artist ngheSi1 = new Artist(1, "thuong");
    Artist ngheSi2 = new Artist(2, "duong");
    Artist ngheSi3 = new Artist(3, "tram");

    // phương thức trả về tất cả các nghệ sỹ
    public ArrayList<Artist> getAllArt() {
        // set Art
        ngheSi1.setAlbums(albumService.getArtThuong());
        ngheSi2.setAlbums(albumService.getArtHuy());
        ngheSi3.setAlbums(albumService.getArtTram());
        // khởi tạo 1 đối tượng array list
        ArrayList<Artist> artLists = new ArrayList<Artist>();
        // thêm các nghệ sĩ vào ARRLIsT
        artLists.add(ngheSi1);
        artLists.add(ngheSi2);
        artLists.add(ngheSi3);
        // trả về Arrlist các nghệ sỹ
        return artLists;
    }

    // phương thức lấy info của nghệ sỹ theo id
    public Artist getInfoArtict(int paramId) {
        ArrayList<Artist> artists = getAllArt();  // tất cả các nghệ sỹ trong 1 array
        Artist infoArtist = new Artist(); // khởi tạo 1 đối tượng để lưu kết quả
        // lọc nghệ sỹ theo id
        for (Artist artistElement : artists) {
            if (artistElement.getId() == paramId) {
                infoArtist = artistElement;
            }
        }
        return infoArtist;
    }

    public Artist getArtistIndext(int index) {
        ArrayList<Artist> artists = getAllArt(); // tất cả các nghệ sỹ trong 1 array
        return artists.get(index);// trả về nghệ sỹ thứ index
    }
}
